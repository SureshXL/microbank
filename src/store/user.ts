import { GetterTree, MutationTree, ActionTree } from 'vuex'
import { State, users } from '../types'

type userGetter = GetterTree<State, any>

export const state: State = {
  user: []
}

export const getters: userGetter = {
  users: state => state.user
  // dones: state => state.todos.filter(todo => todo.checked)
}

export const mutations: MutationTree<State> = {
  addUser(state, newTodo) {
    const todoCopy = Object.assign({}, newTodo)
    state.user.push(todoCopy)
  }
}

export const actions: ActionTree<State, any> = {
  addTodoAsync({commit}, data) {
    console.log('-------------------->',data)
    // fetch('https://jsonplaceholder.typicode.com/posts/' + id)
    //   .then(data => data.json())
    //   .then(item => {
    //     const user: users = {
    //       fullName: item.fullName,
    //       email: item.email,
    //       password:item.password,
    //       dateOfIncorporation:item.dateOfIncorporation
    //     }

    //     commit('addUser', user)
    //   })
  }
}






